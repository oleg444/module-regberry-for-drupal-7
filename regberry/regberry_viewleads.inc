<?php

/**
 * @file
 * ViewLeads hook implementations for the Regberry module.
 */

function regberry_view_leads_callback() {
  $form = drupal_get_form('regberry_viewleads_filter_form');
  $output = drupal_render($form);
  $output .= regberry_viewleads_table();
  return $output;
}

/**
 * Form builder.
 */
function regberry_viewleads_filter_form() {

  $form['#method'] = 'get';

  $form['start'] = [
    '#type'          => 'textfield',
    '#title'         => t('Date start'),
    '#default_value' => !empty($_GET['start']) ? $_GET['start'] : date('d.m.Y', REQUEST_TIME - 86400),
    '#required'      => TRUE,
    '#size'          => 10,
    '#maxlength'     => 10,
  ];

  $form['end'] = [
    '#type'          => 'textfield',
    '#title'         => t('Date end'),
    '#default_value' => !empty($_GET['end']) ? $_GET['end'] : date('d.m.Y', REQUEST_TIME),
    '#required'      => TRUE,
    '#size'          => 10,
    '#maxlength'     => 10,
  ];

  $form['city'] = [
    '#title'   => 'Город',
    '#type'    => 'select',
    '#options' => regberry_getListCity(),
    '#default_value' => !empty($_GET['city']) ? $_GET['city'] : '',
  ];

  $form['submit'] = [
    '#type'  => 'submit',
    '#value' => t('Filter'),
  ];

  $form['#redirect'] = FALSE;
  $form['#attached']['library'][] = ['system', 'ui.datepicker'];
  $form['#attached']['js'][] = drupal_get_path('module', 'regberry') . '/regberry.js';

  return $form;
}

/**
 * Form table
 * @return string
 */
function regberry_viewleads_table() {

  drupal_set_title(variable_get('regberry_view_leads_table_title'));

  $tableHeader = [
    t('Date'),
    t('Full Name'),
    t('Phone'),
    t('E-mail'),
    t('Thumbnail of the scan INN'),
    t('A link to the ZIP'),
    t('Status'),
    t('UUID'),
  ];


  $query = db_select('regberry', 'r')
    ->extend('PagerDefault')
    ->extend('TableSort');
  $query->innerJoin('file_managed', 'f', 'f.fid = r.file_inn');
  $query->fields('r', [
    'created_at',
    'name',
    'phone',
    'mail',
    'link',
    'status',
    'uuid'
  ]);
  $query->fields('f', ['uri']);
  $query->orderBy('created_at', 'DESC');


  if (isset($_GET)) {
    $args = [
      'city'  => FILTER_VALIDATE_INT,
      'start' => FILTER_SANITIZE_STRING,
      'end'   => FILTER_SANITIZE_STRING,
    ];
    $inputs = filter_input_array(INPUT_GET, $args);
    if (!empty($inputs['city'])) {
      $query->condition('r.city', $inputs['city']);
    }
    if (!empty($inputs['start']) && !empty($inputs['end'])) {
      $query->condition('created_at', [
        date('Y-m-d', strtotime($inputs['start'])),
        date('Y-m-d', strtotime($inputs['end'])),
      ], 'BETWEEN');
    }
  }

  $rows = $query->execute();

  $tableData = [];
  foreach ($rows as $row) {
    $tableData[] = [
      format_date(strtotime($row->created_at), 'custom', variable_get('reberry_view_leads_date_format')),
      $row->name,
      $row->phone,
      $row->mail,
      '<img src="' . file_create_url(image_style_path('thumbnail', $row->uri)) . '">',
      '<a href="' . $row->link . '">Ссылка на ZIP</a>',
      $row->status,
      $row->uuid,
    ];
  }

  if (count($tableData) == 0) {
    return 'Ничего не найдено.';
  }

  $output = theme('table', ['header' => $tableHeader, 'rows' => $tableData]);
  $output .= theme('pager');
  return $output;

}

/**
 * Form submit callback.
 * @param $form
 * @param $form_state
 */
function regberry_viewleads_filter_form_submit($form, &$form_state) {
  $form_state['redirect'] = FALSE;
}