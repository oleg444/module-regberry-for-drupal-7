<?php

/**
 * @file
 * Add lead
 */

/**
 * Build Regberry Leads form.
 *
 * @param $form
 * @param $form_state
 * @return array
 */
function regberry_page_form($form, $form_state) {

  $form = [];
  $form['name'] = [
    '#type'       => 'textfield',
    '#title'      => t('Full Name'),
    '#size'       => 80,
    '#required'   => TRUE,
    '#attributes' => ['placeholder' => t('Ivanov Ivan Ivanovich')]
  ];
  $form['phone'] = [
    '#type'       => 'textfield',
    '#title'      => t('Cell phone in Russia'),
    '#size'       => 20,
    '#required'   => TRUE,
    '#attributes' => ['placeholder' => t('+7(999)123-45-67')]
  ];
  $form['mail'] = [
    '#type'       => 'textfield',
    '#title'      => t('E-mail'),
    '#size'       => 20,
    '#required'   => TRUE,
    '#attributes' => ['placeholder' => t('name@domain.ru')]
  ];
  $form['city'] = [
    '#type'     => 'select',
    '#title'    => t('City'),
    '#options'  => regberry_getListCity(),
    '#required' => TRUE,
  ];
  $form['file_inn'] = [
    '#type'        => 'file',
    '#title'       => t('Scan copy of the INN'),
    '#description' => 'Выберите файл с расширением jpeg или png',
  ];
  $form['submit'] = [
    '#type'     => 'submit',
    '#value'    => t('Send'),
    '#required' => TRUE,
  ];
  return $form;
}

/**
 * Form validate callback.
 *
 * @param $form
 * @param $form_state
 */
function regberry_page_form_validate($form, &$form_state) {

  $nameExp = explode(' ', $form_state['values']['name']);
  if (count($nameExp) !== 3) {
    form_set_error('email', t('The name filled in is not correct. The correct format is Ivanov Ivan Ivanovich'));
  }

  if (!preg_match('/^[+][7][(][0-9]{3}[)][0-9]{3}[-][0-9]{2}[-][0-9]{2}$/', $form_state['values']['phone'])) {
    form_set_error('email', t('Phone not valid. The correct format is +7(999)123-45-67'));
  }

  $email = $form_state['values']['mail'];
  if (!valid_email_address($email)) {
    form_set_error('email', t('Email not valid'));
  }

  if (user_load_by_mail($email)) {
    form_set_error('email', t('A user with this email already exists'));
  }

  $validImage = [
    'file_validate_is_image'   => [],
    'file_validate_extensions' => [variable_get('regberry_input_image_format')],
  ];
  if ($file = file_save_upload('file_inn', $validImage, 'public://')) {
    $fileNewName = md5(date('YdmHis') . variable_get('regberry_salt') . $file->filename) . '.' . pathinfo($file->filename, PATHINFO_EXTENSION);
    $file = file_move($file, 'public://' . variable_get('regberry_inn_images_path') . DIRECTORY_SEPARATOR . $fileNewName);
    $form_state['values']['file_inn'] = $file;
  }
  else {
    form_set_error('file_inn', t('File was not uploaded'));
  }
}


/**
 * Edit data After validate
 * @param $form_state
 */
function regberry_lead_alter(&$form_state) {
  // @todo - A possible change in data
}

/**
 * Form submit callback.
 *
 * @param $form
 * @param $form_state
 */

function regberry_page_form_submit($form, &$form_state) {

  drupal_alter('lead', $form_state);

  $arguments = [
    'name'   => $form_state['values']['mail'],
    'pass'   => user_password(),
    'mail'   => $form_state['values']['mail'],
    'status' => 1,
    'roles'  => [DRUPAL_AUTHENTICATED_RID => TRUE]
  ];
  user_save(NULL, $arguments);

  $resultData = regberry_sendlead($form_state);

  $zipLink = regberry_InnScanModify($form_state['values']['file_inn']);

  $params = [
    'name'     => $form_state['values']['name'],
    'phone'    => $form_state['values']['phone'],
    'mail'     => $form_state['values']['mail'],
    'city'     => $form_state['values']['city'],
    'file_inn' => $form_state['values']['file_inn']->fid,
    'status'   => $resultData->status,
    'uuid'     => !empty($resultData->uuid) ? $resultData->uuid : '',
    'link'     => $zipLink,
  ];
  regberry_lead_insert($params);

  drupal_set_message(t('Request has been sent'));
}

/**
 * Insert lead to DB
 * @param $params
 */
function regberry_lead_insert($params) {
  try {
    db_insert('regberry')
      ->fields([
        'name'       => $params['name'],
        'phone'      => $params['phone'],
        'mail'       => $params['mail'],
        'city'       => $params['city'],
        'file_inn'   => $params['file_inn'],
        'status'     => $params['status'],
        'uuid'       => $params['uuid'],
        'link'       => $params['link'],
        'created_at' => date('Y-m-d'),
      ])
      ->execute();
  } catch (Exception $e) {
    watchdog(
      'regberry',
      'Insert lead to DB: @exception',
      ['@exception' => $e]
    );
  }
}

/**
 * Send lead data
 * @param $form_state
 * @return mixed
 */
function regberry_sendlead($form_state) {

  $data = [
    'phone' => $form_state['values']['phone'],
    'name'  => $form_state['values']['name'],
    'mail'  => $form_state['values']['mail'],
    'city'  => $form_state['values']['city'],
  ];

  $curlOptions = [
    CURLOPT_URL            => variable_get('regberry_post_url'),
    CURLOPT_RETURNTRANSFER => TRUE,
    CURLOPT_POST           => TRUE,
    CURLOPT_POSTFIELDS     => http_build_query([$data]),
  ];

  if (strpos(variable_get('regberry_post_url'), 'https')) {
    $curlOptions[CURLOPT_SSL_VERIFYPEER] = FALSE;
  }

  $curl = curl_init();
  curl_setopt_array($curl, $curlOptions);
  $response = curl_exec($curl);
  curl_close($curl);

  $result = json_decode($response);

  if (empty($result->status)) {
    $result = new \stdClass;
    $result->status = 'error';
  }
  return $result;
}

/**
 * Modyfy scan INN
 * Converted to one-bit TIFF with automatic correction of brightness
 * and compress to ZIP
 */
function regberry_InnScanModify($file) {

  $file->status = FILE_STATUS_PERMANENT;
  file_save($file);

  $imageNameExp = explode('.', $file->filename);
  $imageRealPath = drupal_realpath($file->uri);
  $imageConvertName = $imageNameExp[0] . '.' . variable_get('regberry_output_image_format');
  $imageConvertRealPath = drupal_realpath('public://') . DIRECTORY_SEPARATOR . variable_get('regberry_inn_images_path') . '-' . variable_get('regberry_output_image_format') . DIRECTORY_SEPARATOR . $imageConvertName;
  $zipFileName = 'public://' . variable_get('regberry_inn_zip_path') . DIRECTORY_SEPARATOR . $imageNameExp[0] . '.zip';

  $imagick = new Imagick();
  $imagick->readImage($imageRealPath);
  $imagick->brightnessContrastImage(variable_get('regberry_image_brightness'), variable_get('regberry_image_contrast'));
  $imagick->setImageDepth(variable_get('regberry_imagedepth'));
  $imagick->setImageFormat(variable_get('regberry_output_image_format'));
  file_put_contents($imageConvertRealPath, $imagick);

  $image_style_name = 'thumbnail';
  $image_style = image_style_load($image_style_name);
  $destination = image_style_path($image_style_name, $file->uri);
  image_style_create_derivative($image_style, $file->uri, $destination);

  $zip = new ZipArchive();
  try {
    $zip->open(drupal_realpath($zipFileName), ZipArchive::CREATE);
    $zip->addFromString($imageConvertName, $imagick);
    $zip->close();
    return file_create_url($zipFileName);
  } catch (Exception $e) {
    watchdog(
      'regberry',
      'Error create zip: @exception',
      ['@exception' => $e]
    );
  }

}

