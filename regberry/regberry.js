(function ($) {
    Drupal.behaviors.myModule = {
        attach: function (context){
            $('#edit-start', context).datepicker({
                dateFormat: 'dd.mm.yy'
            });

            $('#edit-end', context).datepicker({
                dateFormat: 'dd.mm.yy'
            });

            // $('#edit-start', context).change(function() {
            //     var strDate = $('#edit-start', context).attr('value');
            //     var dateParts = strDate.split('.');
            //
            //     $('#edit-end', context).datepicker('option', 'minDate', new Date(dateParts[2], dateParts[1] - 1, parseInt(dateParts[0]) + 1));
            // });
        }
    }
})(jQuery);

