<?php
/**
 * @file
 * Settings for Regberry.
 */

/**
 * Regberry config form.
 */
function regberry_config_form($form, &$form_state) {
  $form = [];

  $form['regberry_view_leads_table_title'] = [
    '#type'          => 'textfield',
    '#title'         => t('Title of table view leads'),
    '#description'   => t('Table name on page View leads'),
    '#default_value' => variable_get('regberry_view_leads_table_title', 'Заявки на бухгалтерское обслуживание'),
    '#required'      => TRUE,
  ];

  $form['reberry_leads_add_title'] = [
    '#type'          => 'textfield',
    '#title'         => t('Title of page leads add'),
    '#description'   => t('Table of page /leads/add'),
    '#default_value' => variable_get('reberry_leads_add_title', 'Добавить заявку'),
    '#required'      => TRUE,
  ];

  $form['reberry_view_leads_title'] = [
    '#type'          => 'textfield',
    '#title'         => t('Title of page View leads'),
    '#description'   => t('Table of page /leads/list'),
    '#default_value' => variable_get('reberry_view_leads_title', 'Посмотреть заявки'),
    '#required'      => TRUE,
  ];

  $form['reberry_view_leads_date_format'] = [
    '#type'          => 'textfield',
    '#title'         => t('Date format for table'),
    '#description'   => t('Date format for table on page View leads'),
    '#default_value' => variable_get('reberry_view_leads_date_format', 'd.m.Y'),
    '#required'      => TRUE,
  ];

  $form['regberry_salt'] = [
    '#type'          => 'textfield',
    '#title'         => t('Salt'),
    '#description'   => t('Salt for generate path name'),
    '#default_value' => variable_get('regberry_salt', 'djgyuir789d5RF37%$'),
    '#required'      => TRUE,
  ];

  $form['regberry_image_brightness'] = [
    '#type'          => 'textfield',
    '#title'         => t('Image brightness'),
    '#description'   => t('Image brightness for modify scan INN'),
    '#default_value' => variable_get('regberry_image_brightness', '-20'),
    '#required'      => TRUE,
  ];

  $form['regberry_image_contrast'] = [
    '#type'          => 'textfield',
    '#title'         => t('Image contrast'),
    '#description'   => t('Image contrast for modify scan INN'),
    '#default_value' => variable_get('regberry_image_contrast', '0'),
    '#required'      => TRUE,
  ];

  $form['regberry_input_image_format'] = [
    '#type'          => 'textfield',
    '#title'         => t('Image input format'),
    '#description'   => t('Image format for upload scan INN'),
    '#default_value' => variable_get('regberry_input_image_format', 'png jpeg jpg'),
    '#required'      => TRUE,
  ];

  $form['regberry_output_image_format'] = [
    '#type'          => 'textfield',
    '#title'         => t('Image output format'),
    '#description'   => t('Image format for convert uploaded scan INN'),
    '#default_value' => variable_get('regberry_output_image_format', 'tiff'),
    '#required'      => TRUE,
  ];

  $form['regberry_imagedepth'] = [
    '#type'          => 'textfield',
    '#title'         => t('Image depth'),
    '#description'   => t('Image depth for convert uploaded scan INN'),
    '#default_value' => variable_get('regberry_imagedepth', '1'),
    '#required'      => TRUE,
  ];

  $form['regberry_inn_images_path'] = [
    '#type'          => 'textfield',
    '#title'         => t('Image path'),
    '#description'   => t('The path for uploaded scan INN'),
    '#default_value' => variable_get('regberry_inn_images_path', 'inn-images'),
    '#required'      => TRUE,
  ];

  $form['regberry_inn_zip_path'] = [
    '#type'          => 'textfield',
    '#title'         => t('Zip path'),
    '#description'   => t('The path for converted and compressed scan INN'),
    '#default_value' => variable_get('regberry_inn_zip_path', 'inn-zip'),
    '#required'      => TRUE,
  ];

  $form['regberry_post_url'] = [
    '#type'          => 'textfield',
    '#title'         => t('Uuid path'),
    '#description'   => t('The path for POST send request'),
    '#default_value' => variable_get('regberry_post_url', 'http://regberry.loc/_temp/lead.php'),
    '#required'      => TRUE,
  ];

  return system_settings_form($form);
}